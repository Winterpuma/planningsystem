﻿namespace PlanProject.Models
{
	internal class Document
	{
		public string Name { get; set; }
		public string Field { get; set; }
		public string Code { get; set; }

		public int Norm { get; set; }
		public int Amount { get; set; }
		public int Total { get; set; }
	}
}
