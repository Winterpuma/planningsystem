﻿using System;

namespace PlanProject.Models
{
	internal class HierarchyObject
	{
		public int Id { get; set; }
		public int? ParentId { get; set; }
		public string ObjectName { get; set; }
		public int ProjectId { get; set; }
		public virtual HierarchyObject Parent { get; set; }
		public virtual ICollection<HierarchyObject> Children { get; set; }
	}
}
