﻿using PlanProject.Models;
using PlanProject.Repository;
using PlanProject.Views;
using System.Data;

namespace PlanProject.Presenter
{
	internal class MainPresenter
	{
		private readonly IMainForm form;
		private readonly IMainRepository repository;

		private int _currentProject = 1;
		private bool _editHierarchy;
		private TreeNode _currentNode;

		public MainPresenter(IMainForm form, IMainRepository repository)
		{
			this.form = form;
			this.repository = repository;

			form.EditHierarchyEvent += EditHierarchy;
			form.ClickNodeEvent += ClickNode;
			form.DeleteCascadeNodeEvent += CurrentNodeDeleteCascade;
			form.DeleteSingleNodeEvent += CurrentNodeDeleteSingle;
			form.EditNodeEvent += CurrentNodeEdit;
			form.AddChildEvent += CurrentNodeAddChild;

			SetHierarchy();
		}

		private void SetHierarchy()
		{
			var objects = repository.HierarchyRepository.GetAll(_currentProject);
			var top = objects.Where(el => el.ParentId == null).FirstOrDefault();

			if (top == null)
				return;

			var topNode = BuildTreeNode(top);

			form.HierarchyTree.Nodes.Clear();
			form.HierarchyTree.Nodes.Add(topNode);
			form.HierarchyTree.Nodes[0].ExpandAll();
		}

		private TreeNode BuildTreeNode(HierarchyObject hierarchy)
		{
			var node = new TreeNode(hierarchy.ObjectName);
			node.Tag = hierarchy.Id;

			if (hierarchy.Children == null)
				return node;

			foreach (var child in hierarchy.Children)
				node.Nodes.Add(BuildTreeNode(child));

			return node;
		}

		private void EditHierarchy(object? sender, EventArgs e)
		{
			_editHierarchy = !_editHierarchy;

			form.HierarchyTree.BackColor = _editHierarchy ? Color.Azure : Color.White;
		}

		private void ClickNode(object? sender, TreeNodeMouseClickEventArgs e)
		{
			if (_editHierarchy)
			{
				_currentNode = e.Node;
				form.EditMenu.Show(form.HierarchyTree.PointToScreen(e.Location));
			}
			else
			{
				// TODO: navigate in not editing mode, switch table
				string msg = "Редактирование запрещено, а функционал таблицы пока не поддерживается";
				const string caption = "Хм";

				MessageBox.Show(msg, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void CurrentNodeDeleteSingle(object? sender, EventArgs? e)
		{
			repository.HierarchyRepository.DeleteSingle((int)_currentNode.Tag);
			SetHierarchy();
		}

		private void CurrentNodeDeleteCascade(object? sender, EventArgs e)
		{
			repository.HierarchyRepository.DeleteRecursive((int)_currentNode.Tag);
			SetHierarchy();
		}
		private void CurrentNodeAddChild(object? sender, EventArgs e)
		{
			repository.HierarchyRepository.Add((int)_currentNode.Tag, _currentProject, "newChild");
			SetHierarchy();
		}
		private void CurrentNodeEdit(object? sender, EventArgs e)
		{
			using (var form = new InputForm())
			{
				var result = form.ShowDialog();

				if (result == DialogResult.OK)
				{
					repository.HierarchyRepository.Rename((int)_currentNode.Tag, form.ReturnValue);
				}
			}

			SetHierarchy();
		}
	}
}
