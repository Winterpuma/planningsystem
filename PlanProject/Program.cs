using PlanProject.Presenter;
using PlanProject.Repository;
using PlanProject.Views;

namespace PlanProject
{
	internal static class Program
	{
		/// <summary>
		///  The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			// To customize application configuration such as set high DPI settings or default font,
			// see https://aka.ms/applicationconfiguration.
			ApplicationConfiguration.Initialize();

			IMainForm view = new MainForm();
			IMainRepository repository = new MainRepository();

			new MainPresenter(view, repository);
			Application.Run((Form)view);
		}
	}
}