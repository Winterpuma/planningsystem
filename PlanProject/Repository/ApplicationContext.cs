﻿using System.Configuration;
using Microsoft.EntityFrameworkCore;
using PlanProject.Models;

namespace PlanProject.Repository
{
	internal class ApplicationContext : DbContext
	{
		public DbSet<HierarchyObject> Hierarchy { get; set; }

		public ApplicationContext() => Database.EnsureCreated();

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["SuppliesDatabase"].ConnectionString);
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
		}
	}
}
