﻿using Microsoft.EntityFrameworkCore;
using PlanProject.Models;

namespace PlanProject.Repository
{
	internal class HierarchyRepository : IHierarchyRepository
	{
		public void Add(int parentId, int projectId, string name)
		{
			using (var db = new ApplicationContext())
			{
				db.Hierarchy.Add(new HierarchyObject { ParentId = parentId, ProjectId = projectId, ObjectName = name });
				db.SaveChanges();
			}
		}

		public void ChangeParent(int id, int newParentId)
		{
			using (var db = new ApplicationContext())
			{
				db.Hierarchy.Where(item => item.Id == id).First().ParentId = newParentId;
				db.SaveChanges();
			}
		}

		public void DeleteRecursive(int id)
		{
			using (var db = new ApplicationContext())
			{
				db.Hierarchy.Load();

				var objToDelete = db.Hierarchy.Where(item => item.Id == id).First();

				DeleteObjectAndChildren(db, objToDelete);

				db.SaveChanges();
			}
		}

		public void DeleteSingle(int id)
		{
			using (var db = new ApplicationContext())
			{
				var objToDelete = db.Hierarchy.Where(item => item.Id == id).ToList().First();
				var childrenToChange = db.Hierarchy.Where(item => item.ParentId == id);

				foreach (var child in childrenToChange)
				{
					child.ParentId = objToDelete.ParentId;
				}

				db.Hierarchy.Remove(objToDelete);
				db.SaveChanges();
			}
		}

		public void Rename(int id, string newName)
		{
			using (var db = new ApplicationContext())
			{
				var objToRename = db.Hierarchy.Where(item => item.Id == id).First();

				objToRename.ObjectName = newName;

				db.SaveChanges();
			}
		}

		public HierarchyObject? Get(int id)
		{
			using (var db = new ApplicationContext())
			{
				return db.Hierarchy.Where(item => item.Id == id).ToList().FirstOrDefault();
			}
		}

		public ICollection<HierarchyObject> GetAll(int projectId)
		{
			using (var db = new ApplicationContext())
			{
				return db.Hierarchy.Where(item => item.ProjectId == projectId).ToList();
			}
		}

		private void DeleteObjectAndChildren(ApplicationContext db, HierarchyObject obj)
		{
			if (obj.Children != null)
			{
				foreach (var el in obj.Children)
					DeleteObjectAndChildren(db, el);
			}

			db.Hierarchy.Remove(obj);
		}
	}
}
