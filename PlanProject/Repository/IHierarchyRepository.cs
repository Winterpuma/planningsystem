﻿using PlanProject.Models;

namespace PlanProject.Repository
{
	internal interface IHierarchyRepository
	{
		void Add(int parentId, int projectId, string name);
		void ChangeParent(int id, int newParentId);
		void DeleteSingle(int id); // Children are moved to parent
		void DeleteRecursive(int id); // Children are deleted
		void Rename(int id, string newName);
		ICollection<HierarchyObject> GetAll(int projectId);
		HierarchyObject? Get(int id);
	}
}
