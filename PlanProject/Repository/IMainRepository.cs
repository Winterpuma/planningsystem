﻿namespace PlanProject.Repository
{
	internal interface IMainRepository
	{
		IHierarchyRepository HierarchyRepository { get; }
	}
}
