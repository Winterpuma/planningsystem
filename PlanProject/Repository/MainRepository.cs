﻿namespace PlanProject.Repository
{
	internal class MainRepository : IMainRepository
	{
		public IHierarchyRepository HierarchyRepository { get; set; }
		
		public MainRepository()
		{
			HierarchyRepository = new HierarchyRepository();
		}
	}
}
