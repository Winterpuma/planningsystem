-- DROP TABLE Hierarchy;
CREATE TABLE Hierarchy
(
     Id int IDENTITY(1,1) PRIMARY KEY,
     ParentId int null,
     ObjectName NVARCHAR(50) not null,
     ProjectId INT DEFAULT 1,
     FOREIGN KEY (ParentId) REFERENCES Hierarchy(Id)
     -- TODO: Add FK on ProjectID when project table created
);