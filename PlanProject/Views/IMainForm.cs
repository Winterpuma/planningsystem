﻿namespace PlanProject.Views
{
	internal interface IMainForm
	{
		TreeView HierarchyTree { get; }
		ContextMenuStrip EditMenu { get; }

		//Events
		event EventHandler ChooseProjectEvent;
		event EventHandler ChooseStageEvent;
		event EventHandler FilterByFieldEvent;
		event EventHandler HideEmptyEvent;
		event EventHandler ExportEvent;

		event EventHandler EditHierarchyEvent;
		event TreeNodeMouseClickEventHandler ClickNodeEvent;
		event EventHandler EditNodeEvent;
		event EventHandler DeleteSingleNodeEvent;
		event EventHandler DeleteCascadeNodeEvent;
		event EventHandler AddChildEvent;

		//Methods
		void SetTableBindingSource(BindingSource data);
	}
}
