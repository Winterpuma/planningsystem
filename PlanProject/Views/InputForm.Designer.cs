﻿namespace PlanProject.Views
{
	partial class InputForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button_ok = new System.Windows.Forms.Button();
			this.textBox_name = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button_ok
			// 
			this.button_ok.Location = new System.Drawing.Point(96, 60);
			this.button_ok.Name = "button_ok";
			this.button_ok.Size = new System.Drawing.Size(94, 29);
			this.button_ok.TabIndex = 0;
			this.button_ok.Text = "Ok";
			this.button_ok.UseVisualStyleBackColor = true;
			this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
			// 
			// textBox_name
			// 
			this.textBox_name.Location = new System.Drawing.Point(12, 12);
			this.textBox_name.Name = "textBox_name";
			this.textBox_name.Size = new System.Drawing.Size(276, 27);
			this.textBox_name.TabIndex = 1;
			// 
			// InputForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(305, 110);
			this.Controls.Add(this.textBox_name);
			this.Controls.Add(this.button_ok);
			this.Name = "InputForm";
			this.Text = "InputForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Button button_ok;
		private TextBox textBox_name;
	}
}