﻿namespace PlanProject.Views
{
	public partial class InputForm : Form
	{
		public string ReturnValue { get => textBox_name.Text; }

		public InputForm()
		{
			InitializeComponent();
		}

		private void button_ok_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
			Close();
		}
	}
}
