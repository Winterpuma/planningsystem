﻿namespace PlanProject
{
	partial class MainForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.button_chooseProject = new System.Windows.Forms.Button();
			this.button_editHierarchy = new System.Windows.Forms.Button();
			this.button_chooseStage = new System.Windows.Forms.Button();
			this.button_filterField = new System.Windows.Forms.Button();
			this.button_hideEmpty = new System.Windows.Forms.Button();
			this.button_export = new System.Windows.Forms.Button();
			this.treeView = new System.Windows.Forms.TreeView();
			this.label1 = new System.Windows.Forms.Label();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.menu_deleteCascade = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_deleteOne = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_addChild = new System.Windows.Forms.ToolStripMenuItem();
			this.menu_rename = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.contextMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// button_chooseProject
			// 
			this.button_chooseProject.Location = new System.Drawing.Point(26, 31);
			this.button_chooseProject.Name = "button_chooseProject";
			this.button_chooseProject.Size = new System.Drawing.Size(198, 29);
			this.button_chooseProject.TabIndex = 0;
			this.button_chooseProject.Text = "Выбор проекта ...";
			this.button_chooseProject.UseVisualStyleBackColor = true;
			// 
			// button_editHierarchy
			// 
			this.button_editHierarchy.Location = new System.Drawing.Point(26, 399);
			this.button_editHierarchy.Name = "button_editHierarchy";
			this.button_editHierarchy.Size = new System.Drawing.Size(198, 29);
			this.button_editHierarchy.TabIndex = 1;
			this.button_editHierarchy.Text = "Редактировать";
			this.button_editHierarchy.UseVisualStyleBackColor = true;
			// 
			// button_chooseStage
			// 
			this.button_chooseStage.Location = new System.Drawing.Point(26, 66);
			this.button_chooseStage.Name = "button_chooseStage";
			this.button_chooseStage.Size = new System.Drawing.Size(198, 29);
			this.button_chooseStage.TabIndex = 2;
			this.button_chooseStage.Text = "Выбор стадии ...";
			this.button_chooseStage.UseVisualStyleBackColor = true;
			// 
			// button_filterField
			// 
			this.button_filterField.Location = new System.Drawing.Point(236, 31);
			this.button_filterField.Name = "button_filterField";
			this.button_filterField.Size = new System.Drawing.Size(260, 29);
			this.button_filterField.TabIndex = 3;
			this.button_filterField.Text = "Фильтр по нарпавлению ...";
			this.button_filterField.UseVisualStyleBackColor = true;
			// 
			// button_hideEmpty
			// 
			this.button_hideEmpty.Location = new System.Drawing.Point(553, 31);
			this.button_hideEmpty.Name = "button_hideEmpty";
			this.button_hideEmpty.Size = new System.Drawing.Size(175, 29);
			this.button_hideEmpty.TabIndex = 4;
			this.button_hideEmpty.Text = "Скрыть 0";
			this.button_hideEmpty.UseVisualStyleBackColor = true;
			// 
			// button_export
			// 
			this.button_export.Location = new System.Drawing.Point(553, 399);
			this.button_export.Name = "button_export";
			this.button_export.Size = new System.Drawing.Size(175, 29);
			this.button_export.TabIndex = 5;
			this.button_export.Text = "Export";
			this.button_export.UseVisualStyleBackColor = true;
			// 
			// treeView
			// 
			this.treeView.Location = new System.Drawing.Point(30, 137);
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size(194, 256);
			this.treeView.TabIndex = 6;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.label1.Location = new System.Drawing.Point(31, 112);
			this.label1.Name = "label1";
			this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.label1.Size = new System.Drawing.Size(143, 20);
			this.label1.TabIndex = 7;
			this.label1.Text = "Иерархия проекта";
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Location = new System.Drawing.Point(236, 66);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.RowHeadersWidth = 51;
			this.dataGridView1.RowTemplate.Height = 29;
			this.dataGridView1.Size = new System.Drawing.Size(492, 327);
			this.dataGridView1.TabIndex = 8;
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_deleteCascade,
            this.menu_deleteOne,
            this.menu_addChild,
            this.menu_rename});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(211, 128);
			// 
			// menu_deleteCascade
			// 
			this.menu_deleteCascade.Name = "menu_deleteCascade";
			this.menu_deleteCascade.Size = new System.Drawing.Size(210, 24);
			this.menu_deleteCascade.Text = "Удалить каскадно";
			// 
			// menu_deleteOne
			// 
			this.menu_deleteOne.Name = "menu_deleteOne";
			this.menu_deleteOne.Size = new System.Drawing.Size(210, 24);
			this.menu_deleteOne.Text = "Удалить один";
			// 
			// menu_addChild
			// 
			this.menu_addChild.Name = "menu_addChild";
			this.menu_addChild.Size = new System.Drawing.Size(210, 24);
			this.menu_addChild.Text = "Добавить потомка";
			// 
			// menu_rename
			// 
			this.menu_rename.Name = "menu_rename";
			this.menu_rename.Size = new System.Drawing.Size(210, 24);
			this.menu_rename.Text = "Переименовать";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(754, 450);
			this.Controls.Add(this.dataGridView1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.treeView);
			this.Controls.Add(this.button_export);
			this.Controls.Add(this.button_hideEmpty);
			this.Controls.Add(this.button_filterField);
			this.Controls.Add(this.button_chooseStage);
			this.Controls.Add(this.button_editHierarchy);
			this.Controls.Add(this.button_chooseProject);
			this.Name = "MainForm";
			this.Text = "Планирование объема работ";
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.contextMenuStrip.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Button button_chooseProject;
		private Button button_editHierarchy;
		private Button button_chooseStage;
		private Button button_filterField;
		private Button button_hideEmpty;
		private Button button_export;
		private TreeView treeView;
		private Label label1;
		private DataGridView dataGridView1;
		private ContextMenuStrip contextMenuStrip;
		private ToolStripMenuItem menu_deleteCascade;
		private ToolStripMenuItem menu_deleteOne;
		private ToolStripMenuItem menu_addChild;
		private ToolStripMenuItem menu_rename;
	}
}