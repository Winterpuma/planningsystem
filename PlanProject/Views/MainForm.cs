using PlanProject.Views;

namespace PlanProject
{
	public partial class MainForm : Form, IMainForm
	{
		public TreeView HierarchyTree => treeView;
		public ContextMenuStrip EditMenu => contextMenuStrip;

		public event EventHandler ChooseProjectEvent;
		public event EventHandler ChooseStageEvent;
		public event EventHandler FilterByFieldEvent;
		public event EventHandler HideEmptyEvent;
		public event EventHandler ExportEvent;

		public event EventHandler EditHierarchyEvent;
		public event TreeNodeMouseClickEventHandler ClickNodeEvent;
		public event EventHandler EditNodeEvent;
		public event EventHandler DeleteSingleNodeEvent;
		public event EventHandler DeleteCascadeNodeEvent;
		public event EventHandler AddChildEvent;

		public MainForm()
		{
			InitializeComponent();

			button_editHierarchy.Click += (sender, args) => EditHierarchyEvent?.Invoke(sender, args);
			treeView.NodeMouseClick += (sender, args) =>  ClickNodeEvent?.Invoke(sender, args);

			EditMenu.Items[0].Click += (sender, args) => DeleteCascadeNodeEvent?.Invoke(sender, args);
			EditMenu.Items[1].Click += (sender, args) => DeleteSingleNodeEvent?.Invoke(sender, args);
			EditMenu.Items[2].Click += (sender, args) => AddChildEvent?.Invoke(sender, args);
			EditMenu.Items[3].Click += (sender, args) => EditNodeEvent?.Invoke(sender, args);
		}

		public void SetTableBindingSource(BindingSource data) => throw new NotImplementedException();
	}
}